const path = require('path')
require('dotenv').config({
  path: path.resolve(__dirname, '../.env')
})
const readline = require('readline')
const bcrypt = require('bcryptjs')

// Depend du fichier dans src/utils/password
const SALT_ROUND = 10

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

console.log(`La clef secrête est ${process.env.JWT_SECRET}`)
console.log('')

rl.question('Entrez un mot de passe à crypter -> ', answer => {
  const operation = new Promise((resolve, reject) => {
    bcrypt.hash(answer, SALT_ROUND, (err, hash) => (err ? reject(err) : resolve(hash)))
  })

  operation.then(hash => {
    console.log(`Le mot de passe crypté est: ${hash}`)
    rl.close()
  })
})
